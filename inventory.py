import libvirt
import sys

conn = libvirt.openReadOnly('qemu:///system')
if conn == None:
    print ('Failed to open connection to the hypervisor', file=sys.stderr)
    sys.exit(1)

host = conn.getHostname()
if host == None:
    print('Failed to get Hostname', file=sys.stderr)
else:
    print('Hostname is: '+host)

domainIDs = conn.listDomainsID()
if domainIDs == None:
    print('Failed to get a list of Domain IDS', file=sys.stderr)

domainNames = []
if len(domainIDs) != 0:
    for domainID in domainIDs:
        domain = conn.lookupByID(domainID)
        domainNames.append(domain.name())

if len(domainNames) != 0:
    print ("All active and incative domain names: ")
    for domainName in domainNames:
        print(' ' +domainName)
else:
    print('No VMs running')


conn.close()
exit(0)